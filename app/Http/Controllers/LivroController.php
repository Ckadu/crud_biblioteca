<?php

namespace App\Http\Controllers;

use App\Livro;
use Illuminate\Http\Request;

class LivroController extends Controller
{
    public function index()
    {
        $livros = Livro::all();
        return view('livros.index',compact('livros',$livros));

    }

    public function create()
    {
        return view('livros.create');
    }
    
    public function store(Request $request)
    {
        //Validate
        $request->validate([
            'titulo' => 'required|max:50',
            'autor' => 'required|max:50',
            'descricao' => 'required',
        ]);
                    
        $livro = Livro::create(['titulo' => $request->titulo,'autor' => $request->autor,'descricao' => $request->descricao]);
        return redirect('/livros/'.$livro->id);
    }

    public function show(Livro $livro)
    {
        return view('livros.show',compact('livro',$livro));
    }
    
    public function edit(Livro $livro)
    {
        return view('livros.edit',compact('livro',$livro));
    }
    
    public function update(Request $request, Livro $livro)
    {
        //Validate
        $request->validate([
            'titulo' => 'required|max:50',
            'autor' => 'required|max:50',
            'descricao' => 'required',
        ]);
        
        $livro->titulo = $request->titulo;
        $livro->autor = $request->autor;
        $livro->descricao = $request->descricao;
        $livro->save();
        $request->session()->flash('message', 'Livro modificado com sucesso!');
        return redirect('livros');
    }
    
    public function destroy(Request $request, Livro $livro)
    {
        $livro->delete();
        $request->session()->flash('message', 'Livro deletado com Sucesso!');
        return redirect('livros');
    }
}
