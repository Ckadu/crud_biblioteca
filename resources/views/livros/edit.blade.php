@extends('layouts.app')
 
@section('content')

    <link href="{{ asset('css/cadastros.css') }}" rel="stylesheet">
  
    @if ($errors->any())
    <div class="alert alert-danger">
      <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
      </ul>
    </div>
    @endif

    <h1>Editar Dados do Livro</h1>
    <hr>
    <form action="{{url('livros', [$livro->id])}}" method="POST" style="margin-left: 27%;">
       <input type="hidden" name="_method" value="PUT">
       {{ csrf_field() }}
        <div class="form-group">
          <div class="col-sm-4">
            <label for="titulo">Titulo</label>
            <input type="text" value="{{$livro->titulo}}" class="form-control" id="titulo"  name="titulo">            
          </div>
        </div>

        <div class="form-group">
          <div class="col-sm-4">
            <label for="autor">Autor</label>
            <input type="text" value="{{$livro->autor}}" class="form-control" id="autor" name="autor">
          </div>
        </div>
        <div class="form-group">
          <div class="col-sm-8">
            <label for="descricao">Descrição</label>
            <textarea class="form-control" rows="8" placeholder="Descrição do Livro" id="descricao" name="descricao">{{ $livro->descricao }}</textarea>
          </div>
        </div>
        <div class="form-group col-sm-12" id="button" style="text-align: center; margin-top: 3%; margin-left: 8%;">
          <input type="hidden" name="_token" value="{{ csrf_token() }}">
          <input type="submit" name="btn" id="button" class="btn btn-primary" value="ATUALIZAR">
        </div>
    </form>

@endsection