@extends('layouts.app')
 
@section('content')

<link href="{{ asset('css/cadastros.css') }}" rel="stylesheet">

<div class="col-sm-12">
    <div class="col-sm-12" id="voltar">
      <a href="/livros">
        <img id="back" src="/img/voltar.png">
        <button id="voltar">VOLTAR</button>
      </a>
    </div>

    <div class="full-width col-sm-12" id="titulo">
      <strong id="titulo">NOVO LIVRO</strong>
    </div>

     <form action="/livros" method="post" style="margin-left: 27%;">
     {{ csrf_field() }}
      <div class="form-group">
        <div class="col-sm-4">
          <label for="titulo">Titulo</label>
          <input type="text" class="form-control" id="titulo"  name="titulo" placeholder="Titulo do Livro">  
        </div>
      </div>

      <div class="form-group">
        <div class="col-sm-4">
          <label for="autor">Autor</label>
          <input type="text" class="form-control" placeholder="Autor do Livro" id="autor" name="autor">
        </div>
      </div>

      <div class="form-group">
        <div class="col-sm-8">
          <label for="descricao">Descrição</label>
          <textarea class="form-control" placeholder="Descrição do Livro" id="descricao" name="descricao" rows="8"></textarea>
        </div>
      </div>

      @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
      @endif

      <div class="col-sm-12" id="botao" style="text-align: center; margin-top: 3%; margin-left: 8%;">
        <button type="submit" class="btn btn-primary">CADASTRAR</button>  
      </div>

    </form>
</div>
@endsection