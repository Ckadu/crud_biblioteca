@extends('layouts.app')

@section('content')
      
      <link href="{{ asset('css/cadastros.css') }}" rel="stylesheet">

        @if (Session::has('message'))
            <div class="alert alert-info">{{ Session::get('message') }}</div>
        @endif
        
        <div class="col-sm-12" id="tilt">
          <div class="col-sm-9">
            <strong id="titulo">Livros Cadastrados no Sistema</strong>       
          </div>
          <div class="col-sm-2">
            <a href="/livros/create">
              <button id="nc">Novo Livro</button>
            </a>
          </div>
          <div class="col-sm-1" style="margin-left: -2%;">
            <a href="/livros/create">
              <img src="/img/novo-livro.png" id="imgcad">
            </a>
          </div>
        </div>

        <div class="col-sm-12">
          <table class="table">
            <thead class="thead-dark">
              <tr>              
                <th id="th" class="col-sm-3" scope="col">Id</th>
                <th id="th" class="col-sm-3" scope="col">Titulo</th>
                <th id="th" class="col-sm-2" scope="col">Autor</th>
                <th id="th" class="col-sm-2" scope="col">Descrição</th>
                <th id="th" class="col-sm-2" scope="col">Opções</th>
              </tr>
            </thead>
            <tbody>
              @foreach($livros as $livro)
              <tr>
                <td>{{$livro->id}}</td>
                <td>{{$livro->titulo}}</td>
                <td>{{$livro->autor}}</td>
                <td>{{ $livro->descricao}}</td>
                <td>
                  <div class="col-sm-12">                      
                    <a href="{{ URL::to('livros/' . $livro->id . '/edit') }}">
                      <img id="imgbtn" src="/img/edit.png">
                    </a>
                    <form style="margin-left: 20%; margin-top: -30px;" action="{{ url('livros', [$livro->id]) }}" method="POST">
                      <input type="hidden" name="_method" value="DELETE">
                      <input type="hidden" name="_token" value="{{ csrf_token() }}">
                      <button style="width: 50px;" type="submit">
                        <img id="imgbtn" src="/img/excluir.png" style="width: 100%">
                      </button>
                    </form>
                  </div>                  
                </td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
@endsection
