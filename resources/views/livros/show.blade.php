@extends('layouts.app')
 
<link href="{{ asset('css/cadastros.css') }}" rel="stylesheet">

@section('content')
<div class="col-sm-12">

	<div class="col-sm-12" id="voltar">
		<a href="/livros">
			<img id="back" src="/img/voltar.png">
			<button id="voltar">VOLTAR</button>
		</a>
	</div>

    <div id="titulo" class="col-sm-12">
    	<strong id="titulo">Informações do Livro  {{ $livro->titulo }}</strong> 
    </div>
 
 	<div class="col-sm-12"> 		
	    <div id="info" class="text-center col-sm-12">	    	
	    	<div class="col-sm-12" id="info">
	    		<div class="col-sm-3 col-sm-offset-3">		
	    			<strong>Titulo: </strong>
	    		</div>
	    		<div class="col-sm-6">
	    			<label>{{ $livro->titulo }}</label>
	    		</div>
	    	</div>
	    	<div class="col-sm-12" id="info">
		    	<div class="col-sm-3 col-sm-offset-3">
		    		<strong>Autor: </strong>
		    	</div>
		    	<div class="col-sm-6">
		    		<label>{{ $livro->autor }}</label>
		    	</div>
	    	</div>
	    	<div class="col-sm-12" id="info">
	    		<div class="col-sm-3 col-sm-offset-3">
	    			<strong>Descrição: </strong>	    		
	    		</div>
	    		<div class="col-sm-6">
	    			<label>{{ $livro->descricao}}</label>	    			
	    		</div>
	    	</div>
	    </div>
 	</div>
</div>
@endsection