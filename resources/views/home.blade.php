<!doctype html>
<html lang="pt_BR">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta property="og:locale" content="pt_BR">

        <title>Biblioteca</title>

        <link href="{{ asset('css/cadastros.css') }}" rel="stylesheet">

    </head>
    <body>
        <div class="flex-center position-ref full-height">
            <div class="content">
                <div class="title">
                    <strong>BIBLIOTECA</strong>
                </div>

                <div class="col-sm-12 flex-center">
                    <a href="/livros">
                        <button>Entrar</button>
                    </a>
                </div>
            </div>
        </div>
    </body>
</html>
